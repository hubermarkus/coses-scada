# CoSES-SCADA

This repository documents the foundation work to develop an **open-source based Supervisory Control and Data Acquisition (SCADA) system for the sector-coupled microgrid** at the Center for Combined Smart Energy Systems (CoSES) at Technical University Munich (TUM). At CoSES, researchers develop new technologies for the evolution of the energy system. To test and improve those technologies, they perform experiments based on an experimental microgrid. This complex cyber-physical system requires a SCADA system, which allows to control and monitor the
integrated devices centrally and automatically.

To speed up the development of such a SCADA system and to profit from existing know-how in this area, a professional open-source framework was requested. To fulfill this need, a metric was developed to analyze various frameworks based on a market analysis, an expert interview,
and hands-on experience. This research process yield the **Experimental Physics and Industrial Control System (EPICS)** as the most promising candidate for the implementation of a CoSES SCADA system.

Based on EPICS, a first - CoSES-independent - prototype has been developed to document and demonstrate how this framework can be used to implement typical SCADA functionalities, in general. The gained know-how was applied to implement a second - CoSES-specific - prototype as a starting point for the further implementation and operation in the laboratory.

# EPICS

More information about EPICS: https://epics-controls.org/

The EPICS Base repository: https://github.com/epics-base
