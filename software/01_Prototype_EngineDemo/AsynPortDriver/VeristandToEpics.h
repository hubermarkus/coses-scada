/* VeristandToEpics.h
 * Asyn driver that inherits from the asynPortDriver class.
 * Authors: Mark Rivers (main structure and some of the comments), Markus Huber (Veristand and parameter specific things)
 * Created Feb. 5, 2009
 * Modified July 10, 2021 */

#include "asynPortDriver.h"									// Inheritance from asynPortDriver
#using <NationalInstruments.VeriStand.dll>					// Allows types like "NationalInstruments::VeriStand::Error"
#using <NationalInstruments.VeriStand.ClientAPI.dll>		// Allows the Namespace ClientAPI
using namespace NationalInstruments::VeriStand::ClientAPI; 	// Allows IWorkspace2 and IModelManager2	

#define DEFAULT_POLL_TIME .5	// Poll time for the poller thread in seconds							

/* These are the drvInfo strings that are used to identify the parameters.
 * They are used by asyn clients, including standard asyn device support */
#define P_EnginePowerString        		"EnginePower"        
#define P_EnginePower_RBV_String   		"EnginePower_RBV"    
#define P_ActualRPMString          		"ActualRPM"          
#define P_DesiredRPMString         		"DesiredRPM"         
#define P_DesiredRPM_RBV_String    		"DesiredRPM_RBV"     
#define P_EngineTempString         		"EngineTemp"         
#define P_EnvironmentTempString    		"EnvironmentTemp" 
#define P_EnvironmentTemp_RBV_String    "EnvironmentTemp_RBV" 
#define P_IdleSpeedString          		"IdleSpeed"       
#define P_IdleSpeed_RBV_String          "IdleSpeed_RBV"

/** Class that demonstrates the use of the asynPortDriver base class to greatly simplify the task
  * of writing an asyn port driver.
  * This class does a simple simulation of a digital oscilloscope.  It computes a waveform, computes
  * statistics on the waveform, and does callbacks with the statistics and the waveform data itself. 
  * I have made the methods of this class public in order to generate doxygen documentation for them,
  * but they should really all be private. */
class VeristandToEpics : public asynPortDriver {
public:
    VeristandToEpics(const char *portName);
                 
    /* These are the methods that we override from asynPortDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser* pasynUser, epicsFloat64 value);
    
    /* These are the methods that are new to this class */
    virtual void pollerThread(void);
        
protected:
    /** Values used for pasynUser->reason, and indexes into the parameter library. */
    int P_EnginePower;
    int P_EnginePower_RBV;
    int P_ActualRPM;
    int P_DesiredRPM;
    int P_DesiredRPM_RBV;
    int P_EngineTemp;
    int P_EnvironmentTemp;
    int P_EnvironmentTemp_RBV;
    int P_IdleSpeed;
    int P_IdleSpeed_RBV;
 
private:
    /* Factory class, which provides access to the NI VeriStand
	 * system and the various interfaces available in the Execution API. Any code you write using
	 * the ClientAPI must include a Factory constructor to access NI VeriStand. */
	gcroot<Factory^> facRef;
	// to access VeriStand channels
    gcroot<IWorkspace2^> workspace_;
    // to access VeriStand model parameters
    gcroot<IModelManager2^> modelManager_;
    // support/enable the poller thread
    double pollTime_;
    int forceCallback_;
};