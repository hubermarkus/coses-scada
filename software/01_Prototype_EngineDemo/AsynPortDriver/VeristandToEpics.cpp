/*
* VeristandToEpics.cpp
*
* Asyn driver that inherits from the asynPortDriver class.
* Authors:
* Mark Rivers (main structure and some of the comments)
* Markus Huber (Veristand and parameter specific things)
* Created: Feb. 5, 2009, Modified: July 10, 2021 */

#include "stdafx.h"				// optional: for compiler optimization
#include <stdlib.h>				// standard library
#include <string.h>				// for string operations
#include <stdio.h>				// e.g., for printf
#include <iocsh.h>				// for IOC shell programming
#include "VeristandToEpics.h"	// this class' header
// EPICS specific headers
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExport.h>

static const char *driverName="VeristandToEpics";	// name of this driver; needed for debugging
void pollerThreadC(void* drvPvt);					// thread that checks for new values


/** Constructor for the VeristandToEpics class.
  * Calls constructor for the asynPortDriver base class.
  * \param[in] portName The name of the asyn port driver to be created. */
VeristandToEpics::VeristandToEpics(const char *portName) 
   : asynPortDriver(portName, /* Name of this asynPort*/
                    1, /* Number of sub-addresses this driver supports */
                    asynInt32Mask | asynFloat64Mask | asynDrvUserMask, /* Bit mask of standard asyn interfaces the driver supports */
                    asynInt32Mask | asynFloat64Mask,  /* Bit mask if interfaces that will do callbacks to device support */
                    0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0), /* Default stack size*/
    pollTime_(DEFAULT_POLL_TIME),
    forceCallback_(1)
{
    asynStatus status;
    const char *functionName = "VeristandToEpics";

	// Get access to running VeriStand model
   	facRef = gcnew Factory();
    workspace_ = facRef->GetIWorkspace2("127.0.0.1");          // gives access to process variables
    modelManager_ = facRef->GetIModelManager2("127.0.0.1");    // gives access to model Parameters
       
    /* Create parameters.
	*  Parameters:
	*  	const char *name: (...String) is the drvInfoString that records use (at the end of the INP field) to connect
	*	asynParamType type
	*	int *index:	The address of an index that createParam is gonna assign an index number
	*/
    createParam(P_EnginePowerString,        	asynParamInt32,         &P_EnginePower);
    createParam(P_EnginePower_RBV_String,   	asynParamInt32,         &P_EnginePower_RBV);
    createParam(P_ActualRPMString,          	asynParamFloat64,       &P_ActualRPM);
    createParam(P_DesiredRPMString,         	asynParamFloat64,       &P_DesiredRPM);
    createParam(P_DesiredRPM_RBV_String,    	asynParamFloat64,       &P_DesiredRPM_RBV);
    createParam(P_EngineTempString,         	asynParamFloat64,       &P_EngineTemp);
    createParam(P_EnvironmentTempString,    	asynParamFloat64,       &P_EnvironmentTemp);
    createParam(P_EnvironmentTemp_RBV_String,   asynParamFloat64,  		&P_EnvironmentTemp_RBV);
    createParam(P_IdleSpeedString,          	asynParamFloat64,       &P_IdleSpeed);
    createParam(P_IdleSpeed_RBV_String,     	asynParamFloat64,       &P_IdleSpeed_RBV);

    // create the thread that checks for updated channel values in the background
    status = (asynStatus)(epicsThreadCreate("VeristandToEpicsPoller",
                          epicsThreadPriorityLow,
                          epicsThreadGetStackSize(epicsThreadStackMedium),
                          (EPICSTHREADFUNC)::pollerThreadC,
                          this) == NULL);
    
	// logging
	if (status) {
        printf("%s:%s: epicsThreadCreate failure\n", driverName, functionName);
        return;
    }
}


void pollerThreadC(void* drvPvt)
{
    VeristandToEpics* pPvt = (VeristandToEpics*)drvPvt;
    pPvt->pollerThread();
}

/* Basically this means reading values from the device, i.e., from the running VeriStand model.
 * Polls channels and parameters from the running device, i.e., VeriStand model.
 * This enables updateing the process DB according to the state of the field process. */
void VeristandToEpics::pollerThread() 
{
    /* This function runs in a separate thread. It waits for the poll time. */
    // For read-only values and RBVs, only!
    static const char* functionName = "pollerThread";
	
	double crntActualRPM, prevActualRPM = 0.0;
    double crntEngineTemp, prevEngineTemp = 0.0;
    double crntDesiredRPMRBV, prevDesiredRPMRBV = 0.0;
    double crntEnginePowerRBV, prevEnginePowerRBV = 0.0;
    double crntEnvironmentTempRBV, prevEnvironmentTempRBV = 0.0;
    double crntIdleSpeedRBV, prevIdleSpeedRBV = 0.0;
    asynStatus status = asynSuccess;

  
    // TODO: Only run, if IOC is also running!
    while (1) {
        lock();
        
		// Read channel values from running VeriStand model
        workspace_->GetSingleChannelValue("ActualRPM", crntActualRPM);
        workspace_->GetSingleChannelValue("EngineTemp", crntEngineTemp);
        workspace_->GetSingleChannelValue("DesiredRPM", crntDesiredRPMRBV);
        workspace_->GetSingleChannelValue("EnginePower", crntEnginePowerRBV);
        // Read parameter values from running VeriStand model
		modelManager_->GetSingleParameterValue("Controller", "environment temperature (C)", crntEnvironmentTempRBV);
        modelManager_->GetSingleParameterValue("Controller", "idle speed (RPM)", crntIdleSpeedRBV);
        
		// Check, if EPICS records (read-only or RBV) need to be updated
		if (crntActualRPM != prevActualRPM) {
            prevActualRPM = crntActualRPM;
            setDoubleParam(P_ActualRPM, crntActualRPM);
        }
        if (crntEngineTemp != prevEngineTemp) {
            prevEngineTemp = crntEngineTemp;
            setDoubleParam(P_EngineTemp, crntEngineTemp);
        }
        if (crntDesiredRPMRBV != prevDesiredRPMRBV) {
            prevDesiredRPMRBV = crntDesiredRPMRBV;
            setDoubleParam(P_DesiredRPM_RBV, crntDesiredRPMRBV);
        }
        if (crntEnginePowerRBV != prevEnginePowerRBV) {
            prevEnginePowerRBV = crntEnginePowerRBV;
            setIntegerParam(P_EnginePower_RBV, (int)(crntEnginePowerRBV));
        }
        if (crntEnvironmentTempRBV != prevEnvironmentTempRBV) {
            prevEnvironmentTempRBV = crntEnvironmentTempRBV;
            setDoubleParam(P_EnvironmentTemp_RBV, crntEnvironmentTempRBV);
        }
        if (crntIdleSpeedRBV != prevIdleSpeedRBV) {
            prevIdleSpeedRBV = crntIdleSpeedRBV;
            setDoubleParam(P_IdleSpeed_RBV, crntIdleSpeedRBV);
        }
        
		// Actually update records
		status = callParamCallbacks(); // performs callbacks on all records
                
        unlock();
        
		// wait for a specific time (to save resources)
		epicsThreadSleep(pollTime_);

        // If error occurs
        if (status)
            asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
                "%s:%s: ERROR within the pollerThread: status=%d\n",
                driverName, functionName, status);   
    }
}

/** Called when asyn clients call pasynInt32->write(), e.g., when a record in the DB gets changed via an OPI and needs to be written to the device.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus VeristandToEpics::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *paramName;
    const char* functionName = "writeInt32";

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setIntegerParam(function, value);
    
    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

	epicsInt32 setPoint;

    if (function == P_EnginePower) {
        getIntegerParam(P_EnginePower, &setPoint); 		// hier wird Wert aus Record Übernommen
        workspace_->SetSingleChannelValue("EnginePower", setPoint);
    }
    else {
        /* All other parameters just get set in parameter list, no need to
         * act on them here */
    }
    
    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();
    
	// logging
    if (status) 
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, 
                  "%s:%s: status=%d, function=%d, name=%s, value=%d", 
                  driverName, functionName, status, function, paramName, value);
    else        
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
              "%s:%s: function=%d, name=%s, value=%d\n", 
              driverName, functionName, function, paramName, value);
    return status;
}



/** Called when asyn clients call pasynFloat64->write(), e.g., when a record in the DB gets changed via an OPI and needs to be written to the device.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks.
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus VeristandToEpics::writeFloat64(asynUser* pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char* paramName;
    const char* functionName = "writeFloat64";

    /* Set the parameter in the parameter library. */
    status = (asynStatus)setDoubleParam(function, value);

    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

	epicsFloat64 setPoint;
	
    if (function == P_DesiredRPM) {
        getDoubleParam(P_DesiredRPM, &setPoint); 		// hier wird Wert aus Record Übernommen
        workspace_->SetSingleChannelValue("DesiredRPM", setPoint);
    }
    else if (function == P_EnvironmentTemp) {
        getDoubleParam(P_EnvironmentTemp, &setPoint); 		// hier wird Wert aus Record Übernommen
        modelManager_->SetSingleParameterValue("Controller", "environment temperature (C)", setPoint);
    }
    else if (function == P_IdleSpeed) {
        getDoubleParam(P_IdleSpeed, &setPoint); 		// hier wird Wert aus Record Übernommen
        modelManager_->SetSingleParameterValue("Controller", "idle speed (RPM)", setPoint);
    }
    else {
        // All other parameters just get set in parameter list, no need to act on them here
    }

    // Do callbacks so higher layers see any changes
    status = (asynStatus)callParamCallbacks();

	// logging
    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
            "%s:%s: status=%d, function=%d, name=%s, value=%d",
            driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
            "%s:%s: function=%d, name=%s, value=%d\n",
            driverName, functionName, function, paramName, value);
    return status;
}



/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

/** EPICS iocsh callable function to call constructor for the VeristandToEpics class.
  * \param[in] portName The name of the asyn port driver to be created. */
int VeristandToEpicsConfigure(const char *portName)
{
    new VeristandToEpics(portName);
    return(asynSuccess);
}


/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg * const initArgs[] = {&initArg0};
static const iocshFuncDef initFuncDef = {"VeristandToEpicsConfigure",1,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    VeristandToEpicsConfigure(args[0].sval);
}

void VeristandToEpicsRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(VeristandToEpicsRegister);

}
