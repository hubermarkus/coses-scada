/* VeristandToEpics.h
 * Asyn driver that inherits from the asynPortDriver class.
 * Authors: Mark Rivers (main structure and some of the comments), Markus Huber (Veristand and parameter specific things)
 * Created Feb. 5, 2009
 * Modified July 10, 2021 */

#include "asynPortDriver.h"

#using <NationalInstruments.VeriStand.ClientAPI.dll>
#using <NationalInstruments.VeriStand.dll>
#using <System.dll>
#using <ASAM.XIL.Interfaces.dll>
#using <NationalInstruments.VeriStand.DataTypes.dll>
#using <NationalInstruments.VeriStand.WorkspaceMacro.dll>

//using namespace NationalInstruments.VeriStand.ClientAPI;  // does not work like this
using namespace NationalInstruments::VeriStand::ClientAPI;  // seems to work like this

//for the poller thread
#define DEFAULT_POLL_TIME .5

/* These are the drvInfo strings that are used to identify the parameters.
 * They are used by asyn clients, including standard asyn device support */
#define P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TH_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TH_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TL_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TL_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TH_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TL_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TM_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TM_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TH_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TL2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TM_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TM_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_FG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FW-HC_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FW-HC_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_TM_CP_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-CP"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_TM_EG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-EG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_TM_FG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-FG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-HC_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-HC_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_FG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FW-HC_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FW-HC_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_TM_CP_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-CP"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_TM_EG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-EG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_TM_FG_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-FG"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_TM_HC_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-HC_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_EB_S_TM_HC_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-HC_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-CC_RL_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-CC_RL_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_aM_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_aM_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_bM_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_bM_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HW_VL_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HW_VL_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_CC_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-CC_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HC_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_VL_aM_aM"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_VL_bM_bM"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HW_CK_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_CK"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HW_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_HS_S_TM_HW_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_FW-HC_HW_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_FW-HC_HW_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_3_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_3"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_4_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_4"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_5_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_5"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_6_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_6"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_7_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_7"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_8_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_8"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_9_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_9"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_10_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_10"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_11_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_11"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_12_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_12"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_13_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_13"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_BT_14_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_14"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_HW_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_HW_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_1"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_2"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_3"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-PS_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-PS_VL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_FW-HC_HW_anlg"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_FW-HC_HW_cntr"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_readST_S_TM_ST_RL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_TM-ST_RL"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_readST_S_TM_ST_VL_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_TM-ST_VL"       /* asynFloat64, 	r/w */
        
#define P_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/E_gas_consumed"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_E_heat_produced_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/E_heat_produced"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_Efficiency_el_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_el"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_Efficiency_th_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_th"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_Efficiency_total_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_total"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_P_el_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_el_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_P_gas_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_gas_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_CHP_P_heat_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_heat_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_EB_E_gas_consumed_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/E_gas_consumed"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_EB_E_heat_produced_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/E_heat_produced"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_EB_Efficiency_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/Efficiency"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_EB_P_gas_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/P_gas_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_EB_P_heat_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/P_heat_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_E_Circulation_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_Circulation"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_DemHeatHC_consumed"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_DemHeatHW_consumed"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_P_Circulation_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_Circulation_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_DemHeatHC_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_DemHeatHW_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_ST_E_heat_produced_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_ST/E_heat_produced"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_ST_P_heat_is_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_ST/P_heat_is"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_heat_toHWS"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_Storage_BT"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_Storage_HWS"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/P_heat_toHWS"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_SOC_BT_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/SOC_BT"       /* asynFloat64, 	r/w */
#define P_Conversion_SF1_Outports_Calc_TS1_SOC_HWS_String          "Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/SOC_HWS"       /* asynFloat64, 	r/w */



/** Class that demonstrates the use of the asynPortDriver base class to greatly simplify the task
  * of writing an asyn port driver.
  * This class does a simple simulation of a digital oscilloscope.  It computes a waveform, computes
  * statistics on the waveform, and does callbacks with the statistics and the waveform data itself. 
  * I have made the methods of this class public in order to generate doxygen documentation for them,
  * but they should really all be private. */
class VeristandToEpics : public asynPortDriver {
public:
    VeristandToEpics(const char *portName, int maxArraySize);
                 
    /* These are the methods that we override from asynPortDriver */
    virtual asynStatus readFloat64(asynUser *pasynUser, epicsFloat64 *value);
    
    /* These are the methods that are new to this class */
    virtual void pollerThread(void);
        

protected:
    /** Values used for pasynUser->reason, and indexes into the parameter library. */
	int P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1;
    int P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2;
    int P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1;
    int P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1;
    int P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2;
    int P_Conversion_SF1_Outports_read_CHP_S_FG;
    int P_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg;
    int P_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr;
    int P_Conversion_SF1_Outports_read_CHP_S_TM_CP;
    int P_Conversion_SF1_Outports_read_CHP_S_TM_EG;
    int P_Conversion_SF1_Outports_read_CHP_S_TM_FG;
    int P_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL;
    int P_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL;
    int P_Conversion_SF1_Outports_read_EB_S_FG;
    int P_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg;
    int P_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr;
    int P_Conversion_SF1_Outports_read_EB_S_TM_CP;
    int P_Conversion_SF1_Outports_read_EB_S_TM_EG;
    int P_Conversion_SF1_Outports_read_EB_S_TM_FG;
    int P_Conversion_SF1_Outports_read_EB_S_TM_HC_RL;
    int P_Conversion_SF1_Outports_read_EB_S_TM_HC_VL;
    int P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg;
    int P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg;
    int P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr;
    int P_Conversion_SF1_Outports_read_HS_S_TM_CC_VL;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HC_RL;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HW_CK;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HW_RL;
    int P_Conversion_SF1_Outports_read_HS_S_TM_HW_VL;
    int P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg;
    int P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_1;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_2;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_3;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_4;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_5;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_6;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_7;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_8;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_9;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_10;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_11;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_12;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_13;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_BT_14;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL;
    int P_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL;
    int P_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg;
    int P_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr;
    int P_Conversion_SF1_Outports_readST_S_TM_ST_RL;
    int P_Conversion_SF1_Outports_readST_S_TM_ST_VL;

    int P_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed;
    int P_Conversion_SF1_Outports_Calc_CHP_E_heat_produced;
    int P_Conversion_SF1_Outports_Calc_CHP_Efficiency_el;
    int P_Conversion_SF1_Outports_Calc_CHP_Efficiency_th;
    int P_Conversion_SF1_Outports_Calc_CHP_Efficiency_total;
    int P_Conversion_SF1_Outports_Calc_CHP_P_el_is;
    int P_Conversion_SF1_Outports_Calc_CHP_P_gas_is;
    int P_Conversion_SF1_Outports_Calc_CHP_P_heat_is;
    int P_Conversion_SF1_Outports_Calc_EB_E_gas_consumed;
    int P_Conversion_SF1_Outports_Calc_EB_E_heat_produced;
    int P_Conversion_SF1_Outports_Calc_EB_Efficiency;
    int P_Conversion_SF1_Outports_Calc_EB_P_gas_is;
    int P_Conversion_SF1_Outports_Calc_EB_P_heat_is;
    int P_Conversion_SF1_Outports_Calc_HS_E_Circulation;
    int P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed;
    int P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed;
    int P_Conversion_SF1_Outports_Calc_HS_P_Circulation_is;
    int P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is;
    int P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is;
    int P_Conversion_SF1_Outports_Calc_ST_E_heat_produced;
    int P_Conversion_SF1_Outports_Calc_ST_P_heat_is;
    int P_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS;
    int P_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT;
    int P_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS;
    int P_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS;
    int P_Conversion_SF1_Outports_Calc_TS1_SOC_BT;
    int P_Conversion_SF1_Outports_Calc_TS1_SOC_HWS;
 
private:
    /* Our data */
    epicsEventId eventId_;
    // to access VeriStand channels
    gcroot<IWorkspace2^> workspace_;
    // to access VeriStand model parameters
    gcroot<IModelManager2^> modelManager_;
    // support/enable the poller thread
    double pollTime_;
    int forceCallback_;
};
