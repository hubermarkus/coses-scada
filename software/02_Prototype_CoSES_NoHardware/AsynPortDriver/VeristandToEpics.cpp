/*
* VeristandToEpics.cpp
*
* Asyn driver that inherits from the asynPortDriver class.
* Authors:
* Mark Rivers (main structure and some of the comments)
* Markus Huber (Veristand and parameter specific things)
* Created: Feb. 5, 2009, Modified: July 10, 2021 */

#include "stdafx.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <iocsh.h>
#include "VeristandToEpics.h"
#include <epicsExport.h>
#include <iostream>

using namespace System;

#using <NationalInstruments.VeriStand.ClientAPI.dll>
#using <NationalInstruments.VeriStand.dll>
#using <System.dll>
#using <ASAM.XIL.Interfaces.dll>
#using <NationalInstruments.VeriStand.DataTypes.dll>
#using <NationalInstruments.VeriStand.WorkspaceMacro.dll>

using namespace NationalInstruments::VeriStand::ClientAPI;

static const char *driverName="VeristandToEpics";
void pollerThreadC(void* drvPvt);


/** Constructor for the VeristandToEpics class.
  * Calls constructor for the asynPortDriver base class.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] maxPoints The maximum  number of points in the volt and time arrays */
VeristandToEpics::VeristandToEpics(const char *portName, int maxPoints) 
   : asynPortDriver(portName, 
                    1, /* maxAddr */ 
                    asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynDrvUserMask, /* Interface mask */
                    asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask,  /* Interrupt mask */
                    0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0), /* Default stack size*/
    pollTime_(DEFAULT_POLL_TIME),
    forceCallback_(1)
{
    asynStatus status;
    const char *functionName = "VeristandToEpics";

	// for v2e: Get Connection to Veristand w/o own v2eInterface.dll
    gcroot<Factory^> facRef;
    facRef = gcnew Factory();
    workspace_ = facRef->GetIWorkspace2("127.0.0.1");          // gives access to process variables
    modelManager_ = facRef->GetIModelManager2("127.0.0.1");    // gives access to model Parameters

    
    eventId_ = epicsEventCreate(epicsEventEmpty);
    
    // MH: Params are beeing created in the constructor.
    createParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1);
    createParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_FG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_FG);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_TM_CP_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_TM_CP);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_TM_EG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_TM_EG);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_TM_FG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_TM_FG);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL);
    createParam(P_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL);
    createParam(P_Conversion_SF1_Outports_read_EB_S_FG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_FG);
    createParam(P_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg);
    createParam(P_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr);
    createParam(P_Conversion_SF1_Outports_read_EB_S_TM_CP_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_TM_CP);
    createParam(P_Conversion_SF1_Outports_read_EB_S_TM_EG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_TM_EG);
    createParam(P_Conversion_SF1_Outports_read_EB_S_TM_FG_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_TM_FG);
    createParam(P_Conversion_SF1_Outports_read_EB_S_TM_HC_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_TM_HC_RL);
    createParam(P_Conversion_SF1_Outports_read_EB_S_TM_HC_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_EB_S_TM_HC_VL);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg);
    createParam(P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_CC_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_CC_VL);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HC_RL);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_CK_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HW_CK);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HW_RL);
    createParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_HS_S_TM_HW_VL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_1);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_2);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_3_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_3);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_4_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_4);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_5_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_5);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_6_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_6);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_7_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_7);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_8_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_8);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_9_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_9);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_10_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_10);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_11_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_11);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_12_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_12);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_13_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_13);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_14_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_BT_14);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL);
    createParam(P_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL);
    createParam(P_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg_String, asynParamFloat64, &P_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg);
    createParam(P_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr_String, asynParamFloat64, &P_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr);
    createParam(P_Conversion_SF1_Outports_readST_S_TM_ST_RL_String, asynParamFloat64, &P_Conversion_SF1_Outports_readST_S_TM_ST_RL);
    createParam(P_Conversion_SF1_Outports_readST_S_TM_ST_VL_String, asynParamFloat64, &P_Conversion_SF1_Outports_readST_S_TM_ST_VL);

    createParam(P_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_E_heat_produced_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_E_heat_produced);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_el_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_Efficiency_el);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_th_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_Efficiency_th);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_total_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_Efficiency_total);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_P_el_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_P_el_is);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_P_gas_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_P_gas_is);
    createParam(P_Conversion_SF1_Outports_Calc_CHP_P_heat_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_CHP_P_heat_is);
    createParam(P_Conversion_SF1_Outports_Calc_EB_E_gas_consumed_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_EB_E_gas_consumed);
    createParam(P_Conversion_SF1_Outports_Calc_EB_E_heat_produced_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_EB_E_heat_produced);
    createParam(P_Conversion_SF1_Outports_Calc_EB_Efficiency_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_EB_Efficiency);
    createParam(P_Conversion_SF1_Outports_Calc_EB_P_gas_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_EB_P_gas_is);
    createParam(P_Conversion_SF1_Outports_Calc_EB_P_heat_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_EB_P_heat_is);
    createParam(P_Conversion_SF1_Outports_Calc_HS_E_Circulation_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_E_Circulation);
    createParam(P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed);
    createParam(P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed);
    createParam(P_Conversion_SF1_Outports_Calc_HS_P_Circulation_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_P_Circulation_is);
    createParam(P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is);
    createParam(P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is);
    createParam(P_Conversion_SF1_Outports_Calc_ST_E_heat_produced_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_ST_E_heat_produced);
    createParam(P_Conversion_SF1_Outports_Calc_ST_P_heat_is_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_ST_P_heat_is);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_SOC_BT_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_SOC_BT);
    createParam(P_Conversion_SF1_Outports_Calc_TS1_SOC_HWS_String, asynParamFloat64, &P_Conversion_SF1_Outports_Calc_TS1_SOC_HWS);


    /* Set the initial values of some parameters */
    
    /* Create the thread that computes the waveforms in the background */
    status = (asynStatus)(epicsThreadCreate("VeristandToEpicsPoller",
                          epicsThreadPriorityLow,
                          epicsThreadGetStackSize(epicsThreadStackMedium),
                          (EPICSTHREADFUNC)::pollerThreadC,
                          this) == NULL);
    if (status) {
        printf("%s:%s: epicsThreadCreate failure\n", driverName, functionName);
        return;
    }
}


void pollerThreadC(void* drvPvt)
{
    VeristandToEpics* pPvt = (VeristandToEpics*)drvPvt;
    pPvt->pollerThread();
}

void VeristandToEpics::pollerThread() 
{
    /* This function runs in a separate thread. It waits for the poll time. */
    // For read-only values and RBVs, only!
    static const char* functionName = "pollerThread";
    
    double crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1, prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2, prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1, prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2, prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1, prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2, prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1, prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2, prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1, prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2, prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1, prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2, prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_FG, prev_Conversion_SF1_Outports_read_CHP_S_FG = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg, prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr, prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_TM_CP, prev_Conversion_SF1_Outports_read_CHP_S_TM_CP = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_TM_EG, prev_Conversion_SF1_Outports_read_CHP_S_TM_EG = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_TM_FG, prev_Conversion_SF1_Outports_read_CHP_S_TM_FG = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL, prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL, prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_FG, prev_Conversion_SF1_Outports_read_EB_S_FG = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg, prev_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr, prev_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_TM_CP, prev_Conversion_SF1_Outports_read_EB_S_TM_CP = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_TM_EG, prev_Conversion_SF1_Outports_read_EB_S_TM_EG = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_TM_FG, prev_Conversion_SF1_Outports_read_EB_S_TM_FG = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_RL, prev_Conversion_SF1_Outports_read_EB_S_TM_HC_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_VL, prev_Conversion_SF1_Outports_read_EB_S_TM_HC_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg, prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr, prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg, prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr, prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg, prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr, prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg, prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr, prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_CC_VL, prev_Conversion_SF1_Outports_read_HS_S_TM_CC_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_RL, prev_Conversion_SF1_Outports_read_HS_S_TM_HC_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM, prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM, prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_CK, prev_Conversion_SF1_Outports_read_HS_S_TM_HW_CK = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_RL, prev_Conversion_SF1_Outports_read_HS_S_TM_HW_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_VL, prev_Conversion_SF1_Outports_read_HS_S_TM_HW_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg, prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr, prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_1, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_2, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_3, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_3 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_4, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_4 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_5, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_5 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_6, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_6 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_7, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_7 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_8, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_8 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_9, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_9 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_10, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_10 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_11, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_11 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_12, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_12 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_13, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_13 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_14, prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_14 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL, prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL, prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL, prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL, prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1, prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2, prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3, prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3 = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL, prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL = 0.0;
    double crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL, prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL = 0.0;
    double crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg, prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg = 0.0;
    double crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr, prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr = 0.0;
    double crnt_Conversion_SF1_Outports_readST_S_TM_ST_RL, prev_Conversion_SF1_Outports_readST_S_TM_ST_RL = 0.0;
    double crnt_Conversion_SF1_Outports_readST_S_TM_ST_VL, prev_Conversion_SF1_Outports_readST_S_TM_ST_VL = 0.0;

    double crnt_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed, prev_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_E_heat_produced, prev_Conversion_SF1_Outports_Calc_CHP_E_heat_produced = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_el, prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_el = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_th, prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_th = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_total, prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_total = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_P_el_is, prev_Conversion_SF1_Outports_Calc_CHP_P_el_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_P_gas_is, prev_Conversion_SF1_Outports_Calc_CHP_P_gas_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_CHP_P_heat_is, prev_Conversion_SF1_Outports_Calc_CHP_P_heat_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_EB_E_gas_consumed, prev_Conversion_SF1_Outports_Calc_EB_E_gas_consumed = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_EB_E_heat_produced, prev_Conversion_SF1_Outports_Calc_EB_E_heat_produced = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_EB_Efficiency, prev_Conversion_SF1_Outports_Calc_EB_Efficiency = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_EB_P_gas_is, prev_Conversion_SF1_Outports_Calc_EB_P_gas_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_EB_P_heat_is, prev_Conversion_SF1_Outports_Calc_EB_P_heat_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_E_Circulation, prev_Conversion_SF1_Outports_Calc_HS_E_Circulation = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed, prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed, prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_P_Circulation_is, prev_Conversion_SF1_Outports_Calc_HS_P_Circulation_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is, prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is, prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_ST_E_heat_produced, prev_Conversion_SF1_Outports_Calc_ST_E_heat_produced = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_ST_P_heat_is, prev_Conversion_SF1_Outports_Calc_ST_P_heat_is = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS, prev_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT, prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS, prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS, prev_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_SOC_BT, prev_Conversion_SF1_Outports_Calc_TS1_SOC_BT = 0.0;
    double crnt_Conversion_SF1_Outports_Calc_TS1_SOC_HWS, prev_Conversion_SF1_Outports_Calc_TS1_SOC_HWS = 0.0;


    asynStatus status = asynSuccess;

  
    // TODO: Only run, if IOC is also running!
    while (1) {
        lock();
        // Read the inputs
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TH_1", crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TH_2", crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TL_1", crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_FW-HC_TL_2", crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TH_1", crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TL_2", crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TM_1", crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-HC_TM_2", crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TH_1", crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TL2", crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TM_1", crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHN/S_TM-TG_TM_2", crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FG", crnt_Conversion_SF1_Outports_read_CHP_S_FG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FW-HC_anlg", crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_FW-HC_cntr", crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-CP", crnt_Conversion_SF1_Outports_read_CHP_S_TM_CP);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-EG", crnt_Conversion_SF1_Outports_read_CHP_S_TM_EG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-FG", crnt_Conversion_SF1_Outports_read_CHP_S_TM_FG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-HC_RL", crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_CHP/S_TM-HC_VL", crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FG", crnt_Conversion_SF1_Outports_read_EB_S_FG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FW-HC_anlg", crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_FW-HC_cntr", crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-CP", crnt_Conversion_SF1_Outports_read_EB_S_TM_CP);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-EG", crnt_Conversion_SF1_Outports_read_EB_S_TM_EG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-FG", crnt_Conversion_SF1_Outports_read_EB_S_TM_FG);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-HC_RL", crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_EB/S_TM-HC_VL", crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-CC_RL_anlg", crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-CC_RL_cntr", crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_aM_anlg", crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_aM_cntr", crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_bM_anlg", crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HC_RL_bM_cntr", crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HW_VL_anlg", crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_FW-HW_VL_cntr", crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-CC_VL", crnt_Conversion_SF1_Outports_read_HS_S_TM_CC_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_RL", crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_VL_aM_aM", crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HC_VL_bM_bM", crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_CK", crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_CK);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_RL", crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_HS/S_TM-HW_VL", crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_FW-HC_HW_anlg", crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_FW-HC_HW_cntr", crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_1", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_2", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_3", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_3);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_4", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_4);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_5", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_5);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_6", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_6);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_7", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_7);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_8", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_8);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_9", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_9);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_10", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_10);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_11", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_11);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_12", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_12);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_13", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_13);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-BT_14", crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_14);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_HW_RL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_HW_VL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_RL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HC_VL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_1", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_2", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-HWS_3", crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-PS_RL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/read_TS1/S_TM-PS_VL", crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_FW-HC_HW_anlg", crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_FW-HC_HW_cntr", crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_TM-ST_RL", crnt_Conversion_SF1_Outports_readST_S_TM_ST_RL);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/readST/S_TM-ST_VL", crnt_Conversion_SF1_Outports_readST_S_TM_ST_VL);
    
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/E_gas_consumed", crnt_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/E_heat_produced", crnt_Conversion_SF1_Outports_Calc_CHP_E_heat_produced);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_el", crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_el);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_th", crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_th);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/Efficiency_total", crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_total);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_el_is", crnt_Conversion_SF1_Outports_Calc_CHP_P_el_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_gas_is", crnt_Conversion_SF1_Outports_Calc_CHP_P_gas_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_CHP/P_heat_is", crnt_Conversion_SF1_Outports_Calc_CHP_P_heat_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/E_gas_consumed", crnt_Conversion_SF1_Outports_Calc_EB_E_gas_consumed);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/E_heat_produced", crnt_Conversion_SF1_Outports_Calc_EB_E_heat_produced);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/Efficiency", crnt_Conversion_SF1_Outports_Calc_EB_Efficiency);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/P_gas_is", crnt_Conversion_SF1_Outports_Calc_EB_P_gas_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_EB/P_heat_is", crnt_Conversion_SF1_Outports_Calc_EB_P_heat_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_Circulation", crnt_Conversion_SF1_Outports_Calc_HS_E_Circulation);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_DemHeatHC_consumed", crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/E_DemHeatHW_consumed", crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_Circulation_is", crnt_Conversion_SF1_Outports_Calc_HS_P_Circulation_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_DemHeatHC_is", crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_HS/P_DemHeatHW_is", crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_ST/E_heat_produced", crnt_Conversion_SF1_Outports_Calc_ST_E_heat_produced);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_ST/P_heat_is", crnt_Conversion_SF1_Outports_Calc_ST_P_heat_is);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_heat_toHWS", crnt_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_Storage_BT", crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/E_Storage_HWS", crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/P_heat_toHWS", crnt_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/SOC_BT", crnt_Conversion_SF1_Outports_Calc_TS1_SOC_BT);
        workspace_->GetSingleChannelValue("Targets/Simulated System/Simulation Models/Models/Conversion_SF1/Outports/Calc_TS1/SOC_HWS", crnt_Conversion_SF1_Outports_Calc_TS1_SOC_HWS);


        if (crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1 != prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1 = crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1;
            setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1, crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2 != prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2) {
            prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2 = crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2;
            setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2, crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TH_2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1 != prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1 = crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1, crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2 != prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2) {
            prev_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2 = crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2, crnt_Conversion_SF1_Outports_read_CHN_S_FW_HC_TL_2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1 != prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1, crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TH_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2 != prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2, crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TL_2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1 != prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1, crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2 != prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2, crnt_Conversion_SF1_Outports_read_CHN_S_TM_HC_TM_2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1 != prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1, crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TH_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2 != prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2, crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TL2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1 != prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1, crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_1);
        }
        if (crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2 != prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2) {
            prev_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2 = crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2, crnt_Conversion_SF1_Outports_read_CHN_S_TM_TG_TM_2);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_FG != prev_Conversion_SF1_Outports_read_CHP_S_FG) {
            prev_Conversion_SF1_Outports_read_CHP_S_FG = crnt_Conversion_SF1_Outports_read_CHP_S_FG;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_FG, crnt_Conversion_SF1_Outports_read_CHP_S_FG);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg != prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg) {
            prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg = crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg, crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr != prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr) {
            prev_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr = crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr, crnt_Conversion_SF1_Outports_read_CHP_S_FW_HC_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_TM_CP != prev_Conversion_SF1_Outports_read_CHP_S_TM_CP) {
            prev_Conversion_SF1_Outports_read_CHP_S_TM_CP = crnt_Conversion_SF1_Outports_read_CHP_S_TM_CP;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_TM_CP, crnt_Conversion_SF1_Outports_read_CHP_S_TM_CP);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_TM_EG != prev_Conversion_SF1_Outports_read_CHP_S_TM_EG) {
            prev_Conversion_SF1_Outports_read_CHP_S_TM_EG = crnt_Conversion_SF1_Outports_read_CHP_S_TM_EG;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_TM_EG, crnt_Conversion_SF1_Outports_read_CHP_S_TM_EG);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_TM_FG != prev_Conversion_SF1_Outports_read_CHP_S_TM_FG) {
            prev_Conversion_SF1_Outports_read_CHP_S_TM_FG = crnt_Conversion_SF1_Outports_read_CHP_S_TM_FG;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_TM_FG, crnt_Conversion_SF1_Outports_read_CHP_S_TM_FG);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL != prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL) {
            prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL = crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL, crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL != prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL) {
            prev_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL = crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL, crnt_Conversion_SF1_Outports_read_CHP_S_TM_HC_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_FG != prev_Conversion_SF1_Outports_read_EB_S_FG) {
            prev_Conversion_SF1_Outports_read_EB_S_FG = crnt_Conversion_SF1_Outports_read_EB_S_FG;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_FG, crnt_Conversion_SF1_Outports_read_EB_S_FG);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg != prev_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg) {
            prev_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg = crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg, crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr != prev_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr) {
            prev_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr = crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr, crnt_Conversion_SF1_Outports_read_EB_S_FW_HC_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_TM_CP != prev_Conversion_SF1_Outports_read_EB_S_TM_CP) {
            prev_Conversion_SF1_Outports_read_EB_S_TM_CP = crnt_Conversion_SF1_Outports_read_EB_S_TM_CP;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_TM_CP, crnt_Conversion_SF1_Outports_read_EB_S_TM_CP);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_TM_EG != prev_Conversion_SF1_Outports_read_EB_S_TM_EG) {
            prev_Conversion_SF1_Outports_read_EB_S_TM_EG = crnt_Conversion_SF1_Outports_read_EB_S_TM_EG;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_TM_EG, crnt_Conversion_SF1_Outports_read_EB_S_TM_EG);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_TM_FG != prev_Conversion_SF1_Outports_read_EB_S_TM_FG) {
            prev_Conversion_SF1_Outports_read_EB_S_TM_FG = crnt_Conversion_SF1_Outports_read_EB_S_TM_FG;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_TM_FG, crnt_Conversion_SF1_Outports_read_EB_S_TM_FG);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_RL != prev_Conversion_SF1_Outports_read_EB_S_TM_HC_RL) {
            prev_Conversion_SF1_Outports_read_EB_S_TM_HC_RL = crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_TM_HC_RL, crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_VL != prev_Conversion_SF1_Outports_read_EB_S_TM_HC_VL) {
            prev_Conversion_SF1_Outports_read_EB_S_TM_HC_VL = crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_EB_S_TM_HC_VL, crnt_Conversion_SF1_Outports_read_EB_S_TM_HC_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg != prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg = crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg, crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr != prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr = crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr, crnt_Conversion_SF1_Outports_read_HS_S_FW_CC_RL_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg != prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg = crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg, crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr != prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr = crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr, crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_aM_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg != prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg = crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg, crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr != prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr = crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr, crnt_Conversion_SF1_Outports_read_HS_S_FW_HC_RL_bM_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg != prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg = crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg, crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr != prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr) {
            prev_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr = crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr, crnt_Conversion_SF1_Outports_read_HS_S_FW_HW_VL_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_CC_VL != prev_Conversion_SF1_Outports_read_HS_S_TM_CC_VL) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_CC_VL = crnt_Conversion_SF1_Outports_read_HS_S_TM_CC_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_CC_VL, crnt_Conversion_SF1_Outports_read_HS_S_TM_CC_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_RL != prev_Conversion_SF1_Outports_read_HS_S_TM_HC_RL) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HC_RL = crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_RL, crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM != prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM = crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM, crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_aM_aM);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM != prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM = crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM, crnt_Conversion_SF1_Outports_read_HS_S_TM_HC_VL_bM_bM);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_CK != prev_Conversion_SF1_Outports_read_HS_S_TM_HW_CK) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HW_CK = crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_CK;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_CK, crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_CK);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_RL != prev_Conversion_SF1_Outports_read_HS_S_TM_HW_RL) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HW_RL = crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_RL, crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_VL != prev_Conversion_SF1_Outports_read_HS_S_TM_HW_VL) {
            prev_Conversion_SF1_Outports_read_HS_S_TM_HW_VL = crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_HS_S_TM_HW_VL, crnt_Conversion_SF1_Outports_read_HS_S_TM_HW_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg != prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg) {
            prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg = crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg, crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_anlg);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr != prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr) {
            prev_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr = crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr, crnt_Conversion_SF1_Outports_read_TS1_S_FW_HC_HW_cntr);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_1 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_1) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_1 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_1, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_1);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_2 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_2) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_2 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_2, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_2);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_3 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_3) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_3 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_3;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_3, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_3);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_4 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_4) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_4 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_4;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_4, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_4);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_5 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_5) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_5 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_5;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_5, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_5);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_6 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_6) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_6 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_6;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_6, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_6);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_7 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_7) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_7 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_7;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_7, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_7);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_8 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_8) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_8 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_8;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_8, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_8);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_9 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_9) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_9 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_9;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_9, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_9);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_10 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_10) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_10 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_10;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_10, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_10);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_11 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_11) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_11 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_11;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_11, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_11);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_12 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_12) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_12 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_12;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_12, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_12);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_13 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_13) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_13 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_13;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_13, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_13);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_14 != prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_14) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_BT_14 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_14;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_BT_14, crnt_Conversion_SF1_Outports_read_TS1_S_TM_BT_14);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL != prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL != prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_HW_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL != prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL != prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HC_VL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1 != prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_1);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2 != prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_2);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3 != prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3 = crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3, crnt_Conversion_SF1_Outports_read_TS1_S_TM_HWS_3);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL != prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_RL);
        }
        if (crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL != prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL) {
            prev_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL = crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL;
        setDoubleParam(P_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL, crnt_Conversion_SF1_Outports_read_TS1_S_TM_PS_VL);
        }
        if (crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg != prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg) {
            prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg = crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg;
        setDoubleParam(P_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg, crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_anlg);
        }
        if (crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr != prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr) {
            prev_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr = crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr;
        setDoubleParam(P_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr, crnt_Conversion_SF1_Outports_readST_S_FW_HC_HW_cntr);
        }
        if (crnt_Conversion_SF1_Outports_readST_S_TM_ST_RL != prev_Conversion_SF1_Outports_readST_S_TM_ST_RL) {
            prev_Conversion_SF1_Outports_readST_S_TM_ST_RL = crnt_Conversion_SF1_Outports_readST_S_TM_ST_RL;
        setDoubleParam(P_Conversion_SF1_Outports_readST_S_TM_ST_RL, crnt_Conversion_SF1_Outports_readST_S_TM_ST_RL);
        }
        if (crnt_Conversion_SF1_Outports_readST_S_TM_ST_VL != prev_Conversion_SF1_Outports_readST_S_TM_ST_VL) {
            prev_Conversion_SF1_Outports_readST_S_TM_ST_VL = crnt_Conversion_SF1_Outports_readST_S_TM_ST_VL;
        setDoubleParam(P_Conversion_SF1_Outports_readST_S_TM_ST_VL, crnt_Conversion_SF1_Outports_readST_S_TM_ST_VL);
        }



        if (crnt_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed != prev_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed) {
            prev_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed = crnt_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed, crnt_Conversion_SF1_Outports_Calc_CHP_E_gas_consumed);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_E_heat_produced != prev_Conversion_SF1_Outports_Calc_CHP_E_heat_produced) {
            prev_Conversion_SF1_Outports_Calc_CHP_E_heat_produced = crnt_Conversion_SF1_Outports_Calc_CHP_E_heat_produced;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_E_heat_produced, crnt_Conversion_SF1_Outports_Calc_CHP_E_heat_produced);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_el != prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_el) {
            prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_el = crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_el;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_el, crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_el);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_th != prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_th) {
            prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_th = crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_th;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_th, crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_th);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_total != prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_total) {
            prev_Conversion_SF1_Outports_Calc_CHP_Efficiency_total = crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_total;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_Efficiency_total, crnt_Conversion_SF1_Outports_Calc_CHP_Efficiency_total);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_P_el_is != prev_Conversion_SF1_Outports_Calc_CHP_P_el_is) {
            prev_Conversion_SF1_Outports_Calc_CHP_P_el_is = crnt_Conversion_SF1_Outports_Calc_CHP_P_el_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_P_el_is, crnt_Conversion_SF1_Outports_Calc_CHP_P_el_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_P_gas_is != prev_Conversion_SF1_Outports_Calc_CHP_P_gas_is) {
            prev_Conversion_SF1_Outports_Calc_CHP_P_gas_is = crnt_Conversion_SF1_Outports_Calc_CHP_P_gas_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_P_gas_is, crnt_Conversion_SF1_Outports_Calc_CHP_P_gas_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_CHP_P_heat_is != prev_Conversion_SF1_Outports_Calc_CHP_P_heat_is) {
            prev_Conversion_SF1_Outports_Calc_CHP_P_heat_is = crnt_Conversion_SF1_Outports_Calc_CHP_P_heat_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_CHP_P_heat_is, crnt_Conversion_SF1_Outports_Calc_CHP_P_heat_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_EB_E_gas_consumed != prev_Conversion_SF1_Outports_Calc_EB_E_gas_consumed) {
            prev_Conversion_SF1_Outports_Calc_EB_E_gas_consumed = crnt_Conversion_SF1_Outports_Calc_EB_E_gas_consumed;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_EB_E_gas_consumed, crnt_Conversion_SF1_Outports_Calc_EB_E_gas_consumed);
        }
        if (crnt_Conversion_SF1_Outports_Calc_EB_E_heat_produced != prev_Conversion_SF1_Outports_Calc_EB_E_heat_produced) {
            prev_Conversion_SF1_Outports_Calc_EB_E_heat_produced = crnt_Conversion_SF1_Outports_Calc_EB_E_heat_produced;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_EB_E_heat_produced, crnt_Conversion_SF1_Outports_Calc_EB_E_heat_produced);
        }
        if (crnt_Conversion_SF1_Outports_Calc_EB_Efficiency != prev_Conversion_SF1_Outports_Calc_EB_Efficiency) {
            prev_Conversion_SF1_Outports_Calc_EB_Efficiency = crnt_Conversion_SF1_Outports_Calc_EB_Efficiency;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_EB_Efficiency, crnt_Conversion_SF1_Outports_Calc_EB_Efficiency);
        }
        if (crnt_Conversion_SF1_Outports_Calc_EB_P_gas_is != prev_Conversion_SF1_Outports_Calc_EB_P_gas_is) {
            prev_Conversion_SF1_Outports_Calc_EB_P_gas_is = crnt_Conversion_SF1_Outports_Calc_EB_P_gas_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_EB_P_gas_is, crnt_Conversion_SF1_Outports_Calc_EB_P_gas_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_EB_P_heat_is != prev_Conversion_SF1_Outports_Calc_EB_P_heat_is) {
            prev_Conversion_SF1_Outports_Calc_EB_P_heat_is = crnt_Conversion_SF1_Outports_Calc_EB_P_heat_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_EB_P_heat_is, crnt_Conversion_SF1_Outports_Calc_EB_P_heat_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_E_Circulation != prev_Conversion_SF1_Outports_Calc_HS_E_Circulation) {
            prev_Conversion_SF1_Outports_Calc_HS_E_Circulation = crnt_Conversion_SF1_Outports_Calc_HS_E_Circulation;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_E_Circulation, crnt_Conversion_SF1_Outports_Calc_HS_E_Circulation);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed != prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed) {
            prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed = crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed, crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHC_consumed);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed != prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed) {
            prev_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed = crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed, crnt_Conversion_SF1_Outports_Calc_HS_E_DemHeatHW_consumed);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_P_Circulation_is != prev_Conversion_SF1_Outports_Calc_HS_P_Circulation_is) {
            prev_Conversion_SF1_Outports_Calc_HS_P_Circulation_is = crnt_Conversion_SF1_Outports_Calc_HS_P_Circulation_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_P_Circulation_is, crnt_Conversion_SF1_Outports_Calc_HS_P_Circulation_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is != prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is) {
            prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is = crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is, crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHC_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is != prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is) {
            prev_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is = crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is, crnt_Conversion_SF1_Outports_Calc_HS_P_DemHeatHW_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_ST_E_heat_produced != prev_Conversion_SF1_Outports_Calc_ST_E_heat_produced) {
            prev_Conversion_SF1_Outports_Calc_ST_E_heat_produced = crnt_Conversion_SF1_Outports_Calc_ST_E_heat_produced;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_ST_E_heat_produced, crnt_Conversion_SF1_Outports_Calc_ST_E_heat_produced);
        }
        if (crnt_Conversion_SF1_Outports_Calc_ST_P_heat_is != prev_Conversion_SF1_Outports_Calc_ST_P_heat_is) {
            prev_Conversion_SF1_Outports_Calc_ST_P_heat_is = crnt_Conversion_SF1_Outports_Calc_ST_P_heat_is;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_ST_P_heat_is, crnt_Conversion_SF1_Outports_Calc_ST_P_heat_is);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS != prev_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS) {
            prev_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS = crnt_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS, crnt_Conversion_SF1_Outports_Calc_TS1_E_heat_toHWS);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT != prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT) {
            prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT = crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT, crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_BT);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS != prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS) {
            prev_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS = crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS, crnt_Conversion_SF1_Outports_Calc_TS1_E_Storage_HWS);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS != prev_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS) {
            prev_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS = crnt_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS, crnt_Conversion_SF1_Outports_Calc_TS1_P_heat_toHWS);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_SOC_BT != prev_Conversion_SF1_Outports_Calc_TS1_SOC_BT) {
            prev_Conversion_SF1_Outports_Calc_TS1_SOC_BT = crnt_Conversion_SF1_Outports_Calc_TS1_SOC_BT;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_SOC_BT, crnt_Conversion_SF1_Outports_Calc_TS1_SOC_BT);
        }
        if (crnt_Conversion_SF1_Outports_Calc_TS1_SOC_HWS != prev_Conversion_SF1_Outports_Calc_TS1_SOC_HWS) {
            prev_Conversion_SF1_Outports_Calc_TS1_SOC_HWS = crnt_Conversion_SF1_Outports_Calc_TS1_SOC_HWS;
        setDoubleParam(P_Conversion_SF1_Outports_Calc_TS1_SOC_HWS, crnt_Conversion_SF1_Outports_Calc_TS1_SOC_HWS);
        }




        callParamCallbacks();
        
        // TODO: Only do callbacks on records/values that have changed!
        //status = (asynStatus)callParamCallbacks();
        unlock();
        epicsThreadSleep(pollTime_);

        // If error occurs
        if (status)
            asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
                "%s:%s: ERROR within the pollerThread: status=%d\n",
                driverName, functionName, status);   
    }
}

asynStatus VeristandToEpics::readFloat64(asynUser *pasynUser, epicsFloat64 *value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    // needed for logging output
    const char* paramName;
    const char* functionName = "readFloat64";
    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);
    double returnVal = 0;

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus)callParamCallbacks();

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
            "%s:%s: status=%d, function=%d, name=%s, value=%d",
            driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
            "%s:%s: function=%d, name=%s, value=%d\n",
            driverName, functionName, function, paramName, value);
    return status;
}



/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

/** EPICS iocsh callable function to call constructor for the VeristandToEpics class.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] maxPoints The maximum  number of points in the volt and time arrays */
int VeristandToEpicsConfigure(const char *portName, int maxPoints)
{
    new VeristandToEpics(portName, maxPoints);
    return(asynSuccess);
}


/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "max points",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1};
static const iocshFuncDef initFuncDef = {"VeristandToEpicsConfigure",2,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    VeristandToEpicsConfigure(args[0].sval, args[1].ival);
}

void VeristandToEpicsRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(VeristandToEpicsRegister);

}
