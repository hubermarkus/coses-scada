The detailed set-up instructions and the source code for the device driver can be found in the appendix of the thesis.
If you need more information, please let me know: mark[DOT]huber[AT]tum[DOT]de
